'use strict';

var path = require('path'),
  webpack = require('webpack'),
  InlineEnviromentVariablesPlugin = require('inline-environment-variables-webpack-plugin');

module.exports = {
  watch: !!process.env.dev,

  devtool: 'source-map',

  entry: path.resolve(__dirname, '../../client/index.coffee'),
  // context: path.resolve(__dirname, '../../client'),

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../../dist'),
  },

  // resolve: {
  //   alias: {
  //     // 'Auth0': path.resolve(__dirname, 'node_modules/auth0-js/build/auth0.js'),
  //     // 'Auth0Lock': path.resolve(__dirname, 'node_modules/auth0-lock/lib/index.js'),
  //   }
  // },

  plugins: [
    new InlineEnviromentVariablesPlugin([
      'dev',
      'AUTH0_CLIENT_ID',
      'AUTH0_DOMAIN',
    ]),
  ],

  module: {
    loaders: [{
      test: /\.(eot|svg|ttf|woff|woff2|png|jpg|jpeg|gif)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      // loader: 'file-loader?name=/assets/[name].[ext]',
      loader: 'file-loader?name=/assets/[name].[ext]',
    },{
      test: /\.coffee$/,
      loader: 'coffee-loader',
    },{
      test: /\.html$/,
      loader: 'html-loader',
    },{
      test: /\.pug$/,
      loader: 'pug-ng-html-loader',
    },{
      test: /\.json$/,
      loader: 'json-loader',
    },{
      test: /\.css$/,
      loaders: ['style-loader','css-loader'],
    },{
      test: /\.scss$/,
      loaders: ['style-loader','css-loader','sass-loader'],
    },{
      test: /\.styl$/,
      loaders: ['style-loader','css-loader','stylus-loader'],
    },],
  },
};