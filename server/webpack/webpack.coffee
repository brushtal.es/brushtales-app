webpack = require 'webpack'
moment = require 'moment'

# Compiler
webpack require('./webpack.config.js'), (err, stats) ->
  if err
    console.error 'webpack error', err
  else
    console.log moment().format('HH:mm:ss'), '> Webpack Bundled'
