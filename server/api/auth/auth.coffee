passport = require 'passport'
express = require 'express'
router = express.Router()

# Authentication callback point for Auth0 Login
router.get '/callback', passport.authenticate('auth0', {failureRedirect: '/login'}), (req, res) ->
  if !req.user
    throw new Error 'user null'
  res.redirect '/'

# Logout user
router.get '/logout', (req, res) ->
  req.logout()
  res.send ''
  # TODO: If silent logout doesn't work, use redirect method
  # res.redirect '/'

# User's own profile fetch based on login session
router.get '/whoami', (req, res) ->
  res.json (req.user||{})._json || {}

module.exports = router