express = require 'express'
router = express.Router()

router.use '/auth', require './auth/auth.coffee'
router.use '/news', require './news/news.coffee'

module.exports = router