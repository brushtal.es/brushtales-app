passport = require 'passport'
express = require 'express'
app = express()

if !process.env.SESSION_SECRET
  throw new Error 'Missing SESSION_SECRET environment variable'

# Auth0 authentication strategy/user serialization
passport.use require './strategy.auth0.coffee'
passport.serializeUser (user, done) ->
  done null, user
passport.deserializeUser (user, done) ->
  done null, user

# Dependant Middleware (Session storage)
app.use require('cookie-parser')()
app.use require('express-session')({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: !process.env.dev },
  })

# Passport middleware
app.use passport.initialize()
app.use passport.session()

module.exports = app