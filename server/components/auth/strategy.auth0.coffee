Auth0Strategy = require 'passport-auth0'

if !process.env.AUTH0_CLIENT_ID
  throw new Error 'Missing AUTH0_CLIENT_ID environment variable'
if !process.env.AUTH0_DOMAIN
  throw new Error 'Missing AUTH0_DOMAIN environment variable'
if !process.env.AUTH0_SECRET
  throw new Error 'Missing AUTH0_SECRET environment variable'

module.exports = new Auth0Strategy
  domain: process.env.AUTH0_DOMAIN
  clientID: process.env.AUTH0_CLIENT_ID
  clientSecret: process.env.AUTH0_SECRET
  callbackURL: '/callback'
, (accesToken, refreshToken, extraParams, profile, done) ->
  return done null, profile