# Brushtal.es

### Development Setup ###

Install dependancies

```
npm i
npm i -g nodemon
```

Create a `.env` file and set required variables

```
dev=true
PORT=8005
SESSION_SECRET={{PUT_SOME_TEXT_HERE}}
```

Run local development server

```
nodemon -w .env -w index.coffee -w server -e js,coffee ./index.coffee
```