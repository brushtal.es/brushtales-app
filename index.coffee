# require('coffee-script').register()
require('dotenv').config()

path = require 'path'
cluster = require 'cluster'

PORT = process.env.PORT

# Check for required variables
if !PORT
  console.error 'PORT environment variable not set'
  return process.exit 1

if !!process.env.dev
  console.log 'Dev mode activated'

# Master
if cluster.isMaster
  # Spawn workers for each CPU
  # workerCount = require('os').cpus().length
  workerCount = process.env.WEB_CONCURRENCY || require('os').cpus().length
  console.log 'workerCount',workerCount
  for cpu in [0...1] by 1
    cluster.fork()

  # Output on worker death
  cluster.on 'exit', (worker, code, signal) ->
    console.log 'worker '+worker.process.pid+' died'

  cluster.on 'message', (msg) ->
    console.log 'cluster message', msg

  # Webpack bundler
  require './server/webpack/webpack.coffee'

  console.log('server >>> http://localhost:'+PORT+'/');

# Worker
else
  # Build web server
  express = require 'express'
  app = express()
  server = require('http').createServer app

  # Body parser middleware
  app.use require('body-parser').json()

  # Passport authentication middleware(s)
  app.use require './server/components/auth/auth.coffee'

  # Live refresh for dev servers
  if !!process.env.dev
    reload = require 'reload'
    watch = require 'watch'

    reloadServer = reload server,app
    watch.watchTree path.join(__dirname,'dist'), reloadServer.reload

  # Server API
  app.use '/api', require './server/api/routes.coffee'

  # Client static page/scripts
  app.use express.static 'dist'

  # Start Server
  server.listen PORT