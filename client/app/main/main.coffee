angular.module 'app'
  .config ($stateProvider, pages) ->
    $stateProvider.state 'main',
      url: '/'
      template: require './main.pug'
      controller: require './main.ctrl.coffee'
      # resolve:
      #   profile: (whoami) -> whoami()

    pages.items.push
      title: 'Home'
      sref: 'main'
      icon: 'home'