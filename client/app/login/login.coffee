angular.module 'app'
  .config ($stateProvider) ->
    $stateProvider.state 'main.login',
      url: '/login'
      template: require './login.pug'
      controller: require './login.ctrl.coffee'