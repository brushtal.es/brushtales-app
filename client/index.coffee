# Styles
require 'angular-material/angular-material.css'

# Global Dependancies
angular = require 'angular'
router = require '@uirouter/angularjs'

import Auth0 from 'auth0-js'
# import IO from 'socket.io-client'

# Live reload (TODO: TEST)
if !!process.env.dev
  console.info 'Development Mode Activated'

  # Attach live reload script
  head = document.getElementsByTagName('head')[0]
  script = document.createElement('script')
  script.type = 'text/javascript';
  script.src = 'reload/reload.js';
  head.appendChild(script);

app = angular.module 'app', [
  router.default
  require 'angular-animate'
  require 'angular-aria'
  require 'angular-material'
  require 'angular-local-storage'
  require 'angular-jwt'
]

# Config UI-Router
app.config ($urlRouterProvider, $locationProvider) ->
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);

# Local storage service provider
app.config (localStorageServiceProvider) ->
  localStorageServiceProvider
    .setPrefix 'brushtal.es'
    .setStorageCookie 7, '/', true      # 1 week storage, HTTPS only

  if !!process.env.dev
    localStorageServiceProvider.setStorageCookieDomain window.location.hostname

# Resource whitelists
app.config ($sceDelegateProvider) ->
  $sceDelegateProvider.resourceUrlWhitelist [
    'self',
    # TODO: Add API and Asset subdomains
  ]

# Brushtal.es theme
app.config ($mdThemingProvider) ->

  # Brushtal.es theme
  $mdThemingProvider.theme 'Brushtale'
    .primaryPalette 'green'
    .accentPalette 'amber'
    # .accentPalette 'orange'
    # .dark()

  $mdThemingProvider.setDefaultTheme 'Brushtale'
  $mdThemingProvider.enableBrowserColor theme: 'Brushtale'


# Font Icons
app.config ($mdIconProvider) ->
  $mdIconProvider.defaultIconSet('assets/mdi.svg');

# State change failure debugging
app.run ($rootScope, $state) ->
  $state.defaultErrorHandler (err) ->
    console.error '$state change error', err
    $state.go 'main'

  $rootScope.$on '$stateChangeError', (event, toState, toParams, fromState, fromParams, error) ->
    event.preventDefault()
    console.error 'error',error
    $state.go 'main'

# Webpack Require handler
requireAll = (requireContext) ->
  requireContext.keys().map(requireContext)

requireAll require.context "./assets", true, /^\.\/.*\.(css|scss|styl|svg|png|svg)$/
requireAll require.context './app', true, /^\.\/(?!.*(spec|ctrl)).*\.(js|jsx|coffee|litcoffee|css|scss|styl)$/
requireAll require.context './components', true, /^\.\/(?!.*(spec|ctrl)).*\.(js|jsx|coffee|litcoffee|css|scss|styl)$/