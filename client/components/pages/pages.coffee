angular.module 'app'
  .constant 'pages',
    items: []
    hideCheck: (page) ->
      if typeof page.hide == 'function'
        return page.hide()
      return !!page.hide