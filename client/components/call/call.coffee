angular.module 'app'
  .factory 'call', ($q, $http, localStorageService, jwtHelper) ->
    # TODO: allow some global debug switch for production error tracking?
    # TODO: Allow Angular config() level attachment of external logging system(s)
    # TODO: Would benefit from logger.xxx() level tracking of ouptut rather than _log_error()
    logIdentifier = 'Client'
    logger = 
      log: ->
        console.log arguments
        return
      info: ->
        console.info arguments
        return
      warn: ->
        console.warn arguments
        return
      error: ->
        console.error arguments
        return

    callHandler = ->
      @_init()
      this

    callHandler.prototype =
      _init: (options) ->
        # http attached to object so it can be overriden with test mocks

        @_$http = $http
        @_canceler = $q.defer()
        # https://docs.angularjs.org/api/ng/service/$http#usage
        @_options = options or 
          url: ''
          method: 'GET'
          params: {}
          data: null
          headers: {}
          timeout: @_canceler.promise
        # Set URL Default
        @_protocol = ''
        @_host = ''
        @_port = ''
        @_path = ''
        @_update_url()
        # Status/response objects
        @_running = false
        # this._req = null;
        @_promise = null
        # Promise holder
        @_log = false
        @_log_response = false
        @_debug_logging = false

        @_auth0 = false
        this
      _debug: (data) ->
        if !@_debug_logging
          return
        console.info data
        return
      _destroy: ->
        @_debug '_destroy()'
        # Destroys object, pending promises, etc. where needed
        # TODO: Destroy what needs destroying...
        this
      _update_url: ->
        @_debug '_update_url()'
        @_options.url = @_path
        # TODO: replace with @_protocol, @_domain, @_port with all items optional?
        # if @_protocol and @_host and @_port
        #   @_options.url = @_protocol + '://' + @_host + ':' + @_port + '/' + @_path
        if @_host
          @_options.url = @_host + '/' + @_path
        return
      _call: (method) ->
        @_debug '_call()'
        @_debug @_options
        if !@_running
          @_running = true
          @_options.method = method
          # Attach authentication
          if @_auth0
            authResult = localStorageService.get 'auth_result' || {}
            @header 'access_token', authResult.accessToken
            @header 'Authorization', 'Bearer ' + authResult.idToken
          # Log call
          if @_log
            logger.info logIdentifier + ' ' + method, @_options
          # Trigger call
          self = @
          @_promise = $q (resolve, reject) ->
            self._$http(self._options).then (response) ->
              resolve response.data
            , (response) ->
              reject response.data || response.error
          # Local response reactions
          @_promise.then @_call_success.bind(this)
          @_promise.catch @_call_failed.bind(this)
        this
      _call_success: (response) ->
        @_debug '_call_success()'
        if @_log
          if @_log_response
            logger.info logIdentifier + ' Success ' + @_options.method + ' ' + @_options.url, response
          else
            logger.info logIdentifier + ' Success ' + @_options.method + ' ' + @_options.url
        @_running = false
        @_destroy()
        return
      _call_failed: (error) ->
        @_debug '_call_failed()'
        if @_log
          logger.error logIdentifier + ' Failed ' + @_options.method + ' ' + @_options.url, error
        @_running = false
        @_destroy()
        return
      _log_error: (error) ->
        @_debug '_log_error()'
        console.error error
        if @_log
          logger.error logIdentifier + ' Failed ' + @_options.method + ' ' + @_options.url, error
        this
      protocol: (protocol) ->
        @_debug 'protocol()'
        @_protocol = protocol
        @_update_url()
        this
      port: (port) ->
        @_debug 'port()'
        @_port = port
        @_update_url()
        this
      host: (host) ->
        @_debug 'host()'
        @_host = host
        @_update_url()
        this
      path: (path) ->
        @_debug 'path()'
        @_path = path
        @_update_url()
        this
      param: (key, value) ->
        @_debug 'param()'
        @_options.params[key] = value
        this
      params: (obj) ->
        @_debug 'params()'
        obj = obj or {}
        for key of obj
          @_options.params[key] = obj[key]
        this
      header: (key, value) ->
        @_debug 'header()'
        @_options.headers[key] = value
        this
      headers: (obj) ->
        @_debug 'headers()'
        obj = obj or {}
        for key of obj
          @_options.headers[key] = obj[key]
        this
      auth0: ->
        @_debug 'auth0()'
        @_auth0 = true
        this
      log: ->
        @_log = true
        this
      response_log: ->
        @_debug 'response_log()'
        @_log_response = true
        this
      debug: ->
        @_debug_logging = true
        @_debug 'debug()'
        this
      get: ->
        @_debug 'get()'
        if @_running
          return @_log_error('get() failed, call already running')
        @_call 'GET'
      delete: (id) ->
        @_debug 'delete()'
        if id
          @_path += '/' + id
          @_update_url()
        if @_running
          return @_log_error('delete() failed, call already running')
        @_call 'DELETE'
      post: (body) ->
        @_debug 'post()'
        if @_running
          return @_log_error('post() failed, call already running')
        @_options.data = body
        @_call 'POST'
      put: (body) ->
        @_debug 'put()'
        if @_running
          return @_log_error('put() failed, call already running')
        @_options.data = body
        @_call 'PUT'
      patch: (body) ->
        @_debug 'patch()'
        if @_running
          return @_log_error('patch() failed, call already running')
        @_options.data = body
        @_call 'PATCH'
      promise: ->
        @_debug 'promise()'
        if !@_promise
          @get()
        @_promise
      then: (fProc) ->
        @_debug 'then()'
        @promise().then fProc
        this
      catch: (fProc) ->
        @_debug 'catch()'
        @promise().catch fProc
        this
      cancel: ->
        @_debug 'cancel()'
        @_canceler()
        this
    # Public function
    return ->
      new callHandler