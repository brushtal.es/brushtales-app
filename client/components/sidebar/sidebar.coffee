angular.module 'app'
  .directive 'sidebar', ->
    return
      template: require './sidebar.pug'
      controller: require './sidebar.ctrl.coffee'