module.exports = sidebarCtrl = ($scope, $state, $mdSidenav, lockLogin, logout, auth, pages) ->
  $scope.lockLogin = lockLogin
  $scope.logout = logout
  $scope.auth = auth
  $scope.pages = pages

  $scope.close = ->
    $mdSidenav('tools').close()

  $scope.go = (sref) ->
    $scope.close()
    $state.go sref

  $scope.login = ->
    $scope.close()
    lockLogin.show()