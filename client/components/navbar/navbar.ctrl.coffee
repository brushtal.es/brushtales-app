module.exports = navbarCtrl = ($scope, $mdSidenav, lockLogin, logout, auth, pages) ->
  $scope.$mdSidenav = $mdSidenav
  $scope.lockLogin = lockLogin
  $scope.logout = logout
  $scope.auth = auth
  $scope.pages = pages