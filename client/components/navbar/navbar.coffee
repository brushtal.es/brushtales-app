angular.module 'app'
  .directive 'navbar', ->
    return
      template: require './navbar.pug'
      controller: require './navbar.ctrl.coffee'