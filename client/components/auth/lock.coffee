import Auth0Lock from 'auth0-lock'

angular.module 'app'
  .constant 'lockLogin', new Auth0Lock process.env.AUTH0_CLIENT_ID, process.env.AUTH0_DOMAIN,
    # allowSignUp: false
    auth:
      redirectUrl: window.location.origin+'/api/auth/callback'
      # redirect: false
      params:
        scope: 'openid email user_metadata app_metadata picture'

    # allowedConnections: ['Username-Password-Authentication'],
    