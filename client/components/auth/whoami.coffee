angular.module 'app'
  .factory 'whoami', ($http) ->
    return ->
      $http.get 'api/auth/whoami'
        .then (res) ->
          console.log 'res',res
          return res.data
        .catch console.error.bind(console)