angular.module 'app'
  .factory 'logout', ($http, setProfile) ->
    return ->
      setProfile null
      $http.get '/api/auth/logout'
