angular.module 'app'
  
  # Global user data object
  .constant 'auth',
    logged_in: false
    profile: null

  # Update user auth data
  .factory 'setProfile', (auth) ->
    return (profile) ->
      auth.logged_in = !!profile
      auth.profile = profile
      # TODO: attach more items if user roles/etc are needed

  # On page change, update profile
  .run ($rootScope, $http, setProfile) ->
    $rootScope.refreshAuth = ->
      $http.get '/api/auth/whoami'
        .then (res) -> setProfile res.data
        .catch -> setProfile null

    # Run on start
    $rootScope.refreshAuth()

    # Run on state change
    $rootScope.$on '$stateChangeStart', $rootScope.refreshAuth

  # # Authentication state change watch
  # .run ($rootScope, lockLogin, checkAuth, onAuth, localStorageService) ->
  #   # Check if user is authenticated from previous visit
  #   if checkAuth()
  #     onAuth localStorageService.get 'auth_result'

  #   # Watch for authentication success from lock dialog
  #   lockLogin.on 'authenticated', (authResult) ->
  #     onAuth authResult
  #     $rootScope.$apply()

  # .factory 'onAuth', (jwtHelper, setAuth, checkAuth, setProfile) ->
  #   return (authResult) ->
  #     setAuth authResult
  #     setProfile authResult
  #     checkAuth()

  # # Function for checking if user is authenticated
  # .factory 'checkAuth', ($q, localStorageService, setAuth, setProfile) ->
  #   return ->
  #     expiresAt = JSON.parse localStorageService.get 'expires_at'

  #     if expiresAt < new Date().getTime()
  #       # Clear authentication on failure
  #       setAuth()
  #       setProfile()
  #       return false
  #     return true


  # .factory 'logout', (setAuth, setProfile, lockLogin) ->
  #   return ->
  #     setAuth()
  #     setProfile()
  #     lockLogin.logout
  #       # returnTo: window.location.origin
  #       redirect: false

  # .factory 'setAuth', (localStorageService, jwtHelper) ->
  #   return (authResult) ->
  #     if authResult
  #       payload = jwtHelper.decodeToken authResult.idToken
  #       # Save auth details
  #       localStorageService.set 'auth_result', authResult
  #       localStorageService.set 'expires_at', new Date().getTime() + payload.exp
  #     else
  #       # Clear auth details
  #       localStorageService.remove 'auth_result'
  #       localStorageService.remove 'expires_at'

  # .factory 'setProfile', (auth, jwtHelper) ->
  #   return (authResult) ->
  #     authResult = authResult || {}
  #     if !authResult.idToken
  #       auth.logged_in = false
  #       auth.profile = null
  #       return

  #     # Set angular auth object
  #     auth.logged_in = true
  #     auth.profile = jwtHelper.decodeToken authResult.idToken
